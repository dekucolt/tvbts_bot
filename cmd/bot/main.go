package main

import (
	"log"
	"os"
	"time"
	"tvbtsbot/internal/app/entrypoint/telegram/public"

	tele "gopkg.in/telebot.v3"
)

type Config struct {
	TelegramBotToken string
	AllowedHashtags  []string
	TgUserID         int64
}

func main() {
	logger := log.New(os.Stdout, "tvbts-telegram", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	logger.Print("Application is starting...")

	conf := Config{
		TelegramBotToken: "7127571101:AAHjE8U5RO7pznyjzlhCAcQe-EXQ7OcU-50",
		AllowedHashtags:  []string{"#tech", "#marketing", "#sales", "#tools", "#misc"},
		TgUserID:         947788556,
	}

	tgbotSettings := tele.Settings{
		Token:  conf.TelegramBotToken,
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}

	b, err := tele.NewBot(tgbotSettings)
	public.NewBotHandlerGroup(logger, b, conf.AllowedHashtags, conf.TgUserID)
	if err != nil {
		logger.Fatal(err)
	}

	b.Start()
}
