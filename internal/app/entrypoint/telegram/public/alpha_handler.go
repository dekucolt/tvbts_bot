package public

import (
	tele "gopkg.in/telebot.v3"
	"log"
	"slices"
	"strings"
	"sync"
)

type HandlerGroup struct {
	groupId         *int64
	threads         []int
	tgUserId        int64
	mutex           *sync.Mutex
	logger          *log.Logger
	allowedHashtags []string
}

func NewHandlerGroup(logger *log.Logger, allowedHashtags []string, tgUserId int64) *HandlerGroup {
	return &HandlerGroup{
		mutex:           &sync.Mutex{},
		logger:          logger,
		allowedHashtags: allowedHashtags,
		tgUserId:        tgUserId,
	}
}

func (handler *HandlerGroup) SetThreadID(ctx tele.Context) error {
	if handler.groupId != nil {
		return nil
	}

	if ctx.Message() == nil {
		return ctx.Send("There is no thread")
	}

	if !ctx.Message().FromGroup() {
		return ctx.Send("command must be called inside of the group")
	}

	if ctx.Message().Sender == nil || ctx.Message().Sender.ID != handler.tgUserId {
		return ctx.Send("Sender is not owner")
	}

	groupIdVal := ctx.Message().Chat.ID

	handler.groupId = &groupIdVal

	handler.mutex.Lock()
	defer handler.mutex.Unlock()

	handler.threads = append(handler.threads, ctx.Message().ThreadID)

	return ctx.Send("Group ID was set")
}

func (handler *HandlerGroup) AlphaHandler(ctx tele.Context) error {
	logger := handler.logger

	if ctx.Message() == nil {
		if err := ctx.Bot().Delete(ctx.Message()); err != nil {
			logger.Printf("error occured while deleting a message: %v", err)

			return err
		}
	}

	if !ctx.Message().FromGroup() {
		if err := ctx.Bot().Delete(ctx.Message()); err != nil {
			logger.Printf("error occured while deleting a message: %v", err)

			return err
		}
	}

	if !slices.Contains(handler.threads, ctx.Message().ThreadID) {
		logger.Print("not in threads")

		return nil
	}

	var isOccurredOnce bool
	for _, hashtag := range handler.allowedHashtags {
		if strings.Contains(ctx.Message().Text, hashtag) {
			isOccurredOnce = true
		}
	}

	if !isOccurredOnce {
		if err := ctx.Bot().Delete(ctx.Message()); err != nil {
			logger.Printf("error occured while deleting a message: %v", err)

			return err
		}
	}

	return nil
}
