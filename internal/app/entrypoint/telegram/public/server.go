package public

import (
	tele "gopkg.in/telebot.v3"
	"log"
)

func NewBotHandlerGroup(logger *log.Logger, botApi *tele.Bot, allowedHashtags []string, tgUserId int64) {
	group := NewHandlerGroup(logger, allowedHashtags, tgUserId)
	botApi.Handle(tele.OnText, group.AlphaHandler)
	botApi.Handle(tele.OnSticker, group.AlphaHandler)
	botApi.Handle(tele.OnAnimation, group.AlphaHandler)
	botApi.Handle(tele.OnMedia, group.AlphaHandler)

	botApi.Handle("/set_thread_id", group.SetThreadID)
}
